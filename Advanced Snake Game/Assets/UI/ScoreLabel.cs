﻿// Copyright (C) Nathakorn Soontornworachan. All rights reserved.
// Unauthorized copying of this file, via any medium is strictly prohibited.
// Proprietary and confidential. Authored for Advanced Snake Game by pop_nathakorn@hotmail.com.

using TMPro;
using UnityEngine;

namespace UI
{
    public class ScoreLabel : MonoBehaviour
    {
        [SerializeField]
        private TextMeshProUGUI scoreText;

        public int Score
        {
            get { return score; }
            set
            {
                if( score == value )
                    return;

                score = value;
                scoreText.text = $"SCORE : {score}";
            }
        }
        private int score;
    }
}
