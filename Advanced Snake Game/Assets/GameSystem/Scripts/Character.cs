﻿// Copyright (C) Nathakorn Soontornworachan. All rights reserved.
// Unauthorized copying of this file, via any medium is strictly prohibited.
// Proprietary and confidential. Authored for Advanced Snake Game by pop_nathakorn@hotmail.com.

using UnityEngine;

namespace GameSystem
{
    public class Character : MonoBehaviour
    {
        [Header( "Status" )]
        public int Heart;
        public int Sword;
        public int Shield;
        public CharacterType Type;

        [Header( "Components" )]
        [SerializeField]
        private SpriteRenderer typeRenderer;

        public bool HasDied { get => Heart <= 0; }

        public void Refresh( )
        {
            switch( Type )
            {
                case CharacterType.Red:
                    typeRenderer.color = new Color( 1F, 0, 0, 0.125F );
                    break;
                case CharacterType.Green:
                    typeRenderer.color = new Color( 0, 1F, 0, 0.125F );
                    break;
                case CharacterType.Blue:
                    typeRenderer.color = new Color( 0, 0, 1F, 0.125F );
                    break;
            }
        }

        /// <returns>damage deal to target</returns>
        public int Attack( Character target )
        {
            int netSword = Type == target.Type ? Sword * 2 : Sword;
            int damage = Mathf.Max( 1, netSword - target.Shield );
            target.Heart -= damage;
            return damage;
        }

        public override string ToString( )
        {
            return $"(hp={Heart}, atk={Sword}, def={Shield}, {Type})";
        }
    }

    public enum CharacterType : byte
    {
        Red = 0,
        Green = 1,
        Blue = 2
    }
}

