﻿// Copyright (C) Nathakorn Soontornworachan. All rights reserved.
// Unauthorized copying of this file, via any medium is strictly prohibited.
// Proprietary and confidential. Authored for Advanced Snake Game by pop_nathakorn@hotmail.com.

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace GameSystem
{
    public class Avatar : AvatarSlot
    {
        [SerializeField]
        public GameObject avatarSlot;

        [Header( "Speed" )]
        public float MovementInterval = 1F; // in seconds

        [Header( "Speed Modifier" )]
        [SerializeField]
        private float movementIntervalMultiplier = 1F;
        [SerializeField]
        private float movementIntervalAdder = 0F;

        private Direction latestDirection = Direction.Right;
        private Direction currentDirection = Direction.Right;

        private Direction forcedDirection = Direction.Zero;

        private List<Hero> heroes = new List<Hero>( );
        private List<AvatarSlot> avatarSlots = new List<AvatarSlot>( );

        public class OnCollideObjectEvent : UnityEvent<Component> { }
        public OnCollideObjectEvent OnCollideObject = new OnCollideObjectEvent( );

        public int TotalHeart
        {
            get
            {
                int totalHeart = 0;
                foreach( var hero in heroes )
                    totalHeart += hero.Heart;
                return totalHeart;
            }
        }

        private Coroutine movementRoutine;

        public void StartMovement( )
        {
            if( movementRoutine != null )
                return;

            movementRoutine = StartCoroutine( MoveRoutine( ) );
        }

        public void StopMovement( )
        {
            StopCoroutine( movementRoutine );
            movementRoutine = null;
        }

        public void ForceNextDirection( Direction direction )
        {
            forcedDirection = direction;
            UpdateDirection( forcedDirection );
        }

        IEnumerator MoveRoutine( )
        {
            while( true )
            {
                yield return new WaitForSeconds( MovementInterval );

                if( forcedDirection != Direction.Zero )
                {
                    currentDirection = forcedDirection;
                    forcedDirection = Direction.Zero;
                }

                AttemptMove<Component>( currentDirection.X, currentDirection.Y );
                latestDirection = currentDirection;
            }
        }

        public void SpeedUp( )
        {
            MovementInterval = Mathf.Max( MovementInterval * movementIntervalMultiplier + movementIntervalAdder, moveTime );
        }

        public void SetInitialHero( Hero hero )
        {
            Attach( hero );
            heroes.Add( hero );
            avatarSlots.Add( this );
        }

        public IEnumerator AddHero( Hero hero )
        {
            Vector3 lastSlotPosition = avatarSlots[avatarSlots.Count - 1].transform.position;

            var destination = hero.transform.position;
            var avatarSlotObject = Instantiate( avatarSlot, lastSlotPosition, Quaternion.identity );

            var newSlot = avatarSlotObject.GetComponent<AvatarSlot>( );
            newSlot.Attach( hero );
            newSlot.SetMoveTime( moveTime );
            yield return SmoothMovement( destination );

            heroes.Add( hero );
            avatarSlots.Add( newSlot );
        }

        public Hero GetLeadingHero( )
        {
            if( heroes.Count < 1 )
                return null;
            return heroes[0];
        }

        public bool TryRemoveTheLastHero( )
        {
            if( heroes.Count <= 1 )
                return false;

            int lastIndex = heroes.Count - 1;
            var lastSlot = avatarSlots[lastIndex];
            heroes.RemoveAt( lastIndex );
            avatarSlots.RemoveAt( lastIndex );

            Destroy( lastSlot.gameObject );

            lastIndex = heroes.Count - 1;

            return true;
        }

        public bool TryRemoveTheFirstHero( )
        {
            if( heroes.Count <= 1 )
                return false;

            var exLeader = heroes[0];
            heroes.RemoveAt( 0 );
            Destroy( exLeader.gameObject );

            for( int i = 0 ; i < heroes.Count ; i++ )
                avatarSlots[i].Attach( heroes[i] );

            int lastIndex = avatarSlots.Count - 1;
            var lastSlot = avatarSlots[lastIndex];
            avatarSlots.RemoveAt( lastIndex );
            Destroy( lastSlot.gameObject );

            return true;
        }

        public Direction RandomDirection( )
        {
            List<Direction> safeDirections = new List<Direction>( );
            List<Direction> dangerDirections = new List<Direction>( );
            List<Direction> mortalDirections = new List<Direction>( );

            var layerMask = LayerMask.GetMask( "HeroLine", "Blocking" );
            Vector2 lastDir = latestDirection;
            Vector3 right = Vector3.Cross( lastDir, Vector3.forward ).normalized;
            Vector3 left = Vector3.Cross( lastDir, Vector3.back ).normalized;

            var start = transform.position;
            var end = transform.position + right;
            var hit = Physics2D.Linecast( start, end, layerMask );
            if( hit )
            {
                if( hit.collider.tag == "HeroLine" )
                    mortalDirections.Add( (Vector2)right );
                else
                    dangerDirections.Add( (Vector2)right );
            }
            else
            {
                safeDirections.Add( (Vector2)right );
            }

            end = transform.position + left;
            hit = Physics2D.Linecast( start, end, layerMask );
            if( hit )
            {
                if( hit.collider.tag == "HeroLine" )
                    mortalDirections.Add( (Vector2)left );
                else
                    dangerDirections.Add( (Vector2)left );
            }
            else
            {
                safeDirections.Add( (Vector2)right );
            }

            if( safeDirections.Count > 0 )
                return safeDirections[UnityEngine.Random.Range( 0, safeDirections.Count )];

            if( dangerDirections.Count > 0 )
                return dangerDirections[UnityEngine.Random.Range( 0, dangerDirections.Count )];

            return mortalDirections[UnityEngine.Random.Range( 0, mortalDirections.Count )];
        }

        public override IEnumerator SmoothMovement( Vector3 end )
        {
            for( int i = 1 ; i < heroes.Count ; i++ )
                avatarSlots[i].StartCoroutine( avatarSlots[i].SmoothMovement( avatarSlots[i - 1].transform.position ) );

            return base.SmoothMovement( end );
        }

        public void UpdateDirection( Direction direction )
        {
            if( direction.IsOpposite( latestDirection ) || direction == Direction.Zero )
                return;

            currentDirection = direction;
        }

        public void SwitchLeadingHero( SwitchHeroDirection direction )
        {
            switch( direction )
            {
                case SwitchHeroDirection.Normal:
                    {
                        var leadingHero = heroes[0];
                        heroes.RemoveAt( 0 );
                        heroes.Add( leadingHero );
                    }
                    break;
                default:
                    {
                        int lastIndex = heroes.Count - 1;
                        var lastHero = heroes[lastIndex];
                        heroes.RemoveAt( lastIndex );
                        heroes.Insert( 0, lastHero );
                    }
                    break;
            }

            for( int i = 0 ; i < heroes.Count ; i++ )
                avatarSlots[i].Attach( heroes[i] );
        }

        protected override void OnCantMove<T>( T component )
        {
            OnCollideObject.Invoke( component );
        }
    }

    public enum SwitchHeroDirection
    {
        Normal, Reverse
    }
}
