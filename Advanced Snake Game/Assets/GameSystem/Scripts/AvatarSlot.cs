﻿// Copyright (C) Nathakorn Soontornworachan. All rights reserved.
// Unauthorized copying of this file, via any medium is strictly prohibited.
// Proprietary and confidential. Authored for Advanced Snake Game by pop_nathakorn@hotmail.com.

namespace GameSystem
{
    public class AvatarSlot : MovableObject
    {
        public void Attach( Hero hero )
        {
            hero.tag = "Untagged";
            hero.gameObject.layer = 0;
            hero.transform.position = transform.position;
            hero.transform.SetParent( transform );
        }

        protected override void OnCantMove<T>( T component )
        {

        }
    }
}
