﻿// Copyright (C) Nathakorn Soontornworachan. All rights reserved.
// Unauthorized copying of this file, via any medium is strictly prohibited.
// Proprietary and confidential. Authored for Advanced Snake Game by pop_nathakorn@hotmail.com.

using UnityEngine;

namespace GameSystem
{
    public static class Extensions
    {
        public static Vector2 Round( this Vector2 vector2 )
        {
            vector2.x = Mathf.RoundToInt( vector2.x );
            vector2.y = Mathf.RoundToInt( vector2.y );

            return vector2;
        }
    }
}
