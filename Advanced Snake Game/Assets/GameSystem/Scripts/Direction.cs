﻿// Copyright (C) Nathakorn Soontornworachan. All rights reserved.
// Unauthorized copying of this file, via any medium is strictly prohibited.
// Proprietary and confidential. Authored for Advanced Snake Game by pop_nathakorn@hotmail.com.

using UnityEngine;

namespace GameSystem
{
    public struct Direction
    {
        public int X;
        public int Y;

        public static Direction Zero = new Direction( 0, 0 );
        public static Direction Left = new Direction( -1, 0 );
        public static Direction Right = new Direction( 1, 0 );
        public static Direction Up = new Direction( 0, 1 );
        public static Direction Down = new Direction( 0, -1 );

        public static Direction operator +( Direction a, Direction b ) => new Direction( Mathf.Clamp( a.X + b.X, -1, 1 ), Mathf.Clamp( a.Y + b.Y, -1, 1 ) );
        public static bool operator ==( Direction a, Direction b ) => a.X == b.X && a.Y == b.Y;
        public static bool operator !=( Direction a, Direction b ) => a.X != b.X || a.Y != b.Y;

        public static implicit operator Vector2( Direction direction ) => new Vector2( direction.X, direction.Y );
        public static implicit operator Direction( Vector2 vector2 ) => new Direction( vector2 );

        public Direction(int x, int y)
        {
            X = x;
            Y = y;
        }

        public Direction( Vector2 vector2 )
        {
            X = (int)vector2.x;
            Y = (int)vector2.y;
        }

        public bool IsOpposite( Direction direction )
        {
            if( direction == Zero || this == Zero )
                return false;

            return direction + this == Zero;
        }

        public override string ToString( )
        {
            return $"({X}, {Y})";
        }
    }
}
