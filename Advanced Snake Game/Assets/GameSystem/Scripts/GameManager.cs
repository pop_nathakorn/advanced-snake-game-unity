﻿// Copyright (C) Nathakorn Soontornworachan. All rights reserved.
// Unauthorized copying of this file, via any medium is strictly prohibited.
// Proprietary and confidential. Authored for Advanced Snake Game by pop_nathakorn@hotmail.com.

using System;
using System.Collections;
using UI;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace GameSystem
{
    public class GameManager : MonoBehaviour
    {
        [SerializeField]
        private Camera mainCamera;
        [SerializeField]
        private BoardManager boardManager;
        [SerializeField]
        private GameInput gameInput;
        [SerializeField]
        private float characterSpawnTime = 1F;
        [SerializeField]
        private float combatResolveTime = 0.5F;

        [Header( "UI Component" )]
        [SerializeField]
        private ScoreLabel scoreLabel;
        [SerializeField]
        private GameOverPanel gameOverPanel;

        private Avatar avatar;

        private Component collidingObject;

        private Coroutine levelLoop;
        private Coroutine spawnCharacterRoutine;
        private Coroutine updateDirectionRoutine;
        public static GameManager Instance
        {
            get
            {
                if( s_Instance == null )
                    new GameObject( "GameManager", typeof( GameManager ) );

                return s_Instance;
            }
        }
        private static GameManager s_Instance;

        private int score;

        protected virtual void Awake( )
        {
            // Make sure this singleton have only one at a time
            if( s_Instance != null )
            {
                Destroy( this );
            }
            else
            {
                s_Instance = gameObject.GetComponent<GameManager>( );
            }
        }

        protected virtual void OnDestroy( )
        {
            s_Instance = null;
        }

        private void Start( )
        {
            boardManager.BoardSetup( );

            gameOverPanel.onRetryButtonClicked.AddListener( ResetLevel );

            SetupCamera( );

            levelLoop = StartCoroutine( LevelLoop( ) );
        }

        void ResetLevel( )
        {
            SceneManager.LoadScene( "Level" );
        }

        void SetupCamera( )
        {
            // Set camera to the center of board
            float x = ( boardManager.Rows - 1 ) / 2F;
            float y = ( boardManager.Column - 1 ) / 2F;
            mainCamera.transform.position = new Vector3( x, y, -10 );

            // Set camera to see the whole board
            mainCamera.orthographicSize = ( boardManager.Rows / 2 ) + 3;
        }

        public void Instantiate( ) { }

        IEnumerator LevelLoop()
        {
            bool gameHasEnded = false;
            score = 0;
            spawnCharacterRoutine = StartCoroutine( SpawnCharacterRoutine( ) );

            avatar = boardManager.SpawnAvatar( 3, 2, 2, CharacterType.Red );
            avatar.OnCollideObject.AddListener( OnAvatarCollideObject );

            scoreLabel.gameObject.SetActive( true );

            while( !gameHasEnded )
            {
                // Reset colliding object
                collidingObject = null;

                StartAvatarMovement( );

                // waiting for collide something
                while( collidingObject == null )
                    yield return null;

                StopAvatarMovement( );

                switch( collidingObject.tag )
                {
                    case "Hero":
                        var hero = collidingObject.GetComponent<Hero>( );
                        yield return ProcessHeroColliding( hero );
                        break;
                    case "Enemy":
                        var enemy = collidingObject.GetComponent<Enemy>( );
                        yield return ProcessEnemyColliding( enemy, ( result ) => { gameHasEnded = result; } );
                        break;
                    case "HeroLine":
                        gameHasEnded = true;
                        break;
                    case "Wall":
                        var wall = collidingObject.GetComponent<Tile>( );
                        Direction forceDirection = Direction.Zero;
                        yield return HitTheWallAndRandomDirection( wall, ( result, direction ) =>
                        {
                            gameHasEnded = result;
                            forceDirection = direction;
                        } );
                        if( !gameHasEnded )
                        {
                            avatar.ForceNextDirection( forceDirection );
                        }
                        break;
                    default:
                        Debug.LogError( $"Colliding {collidingObject.tag} not implemented!" );
                        yield return null;
                        break;
                }
            }

            scoreLabel.gameObject.SetActive( false );
            gameOverPanel.Score = score;
            gameOverPanel.gameObject.SetActive( true );

            StopCoroutine( spawnCharacterRoutine );
        }

        void StartAvatarMovement( )
        {
            if( updateDirectionRoutine == null )
                updateDirectionRoutine = StartCoroutine( FeedInputToAvatar( ) );

            avatar.StartMovement( );
        }

        void StopAvatarMovement( )
        {
            avatar.StopMovement( );

            StopCoroutine( updateDirectionRoutine );
            updateDirectionRoutine = null;
        }

        IEnumerator FeedInputToAvatar( )
        {
            while(true)
            {
                avatar.UpdateDirection( new Direction( gameInput.horizontal, gameInput.vertical ) );

                if( Input.GetKeyDown( KeyCode.E ) )
                    avatar.SwitchLeadingHero( SwitchHeroDirection.Normal);
                else if( Input.GetKeyDown( KeyCode.Q ) )
                    avatar.SwitchLeadingHero( SwitchHeroDirection.Reverse );

                yield return null;
            }
        }

        IEnumerator SpawnCharacterRoutine( )
        {
            while( true )
            {
                yield return new WaitForSeconds( characterSpawnTime );
                RandomSpawnCharacter( );
                
            }
        }

        void RandomSpawnCharacter( )
        {
            int side = UnityEngine.Random.Range( 0, 2 );
            int heart = UnityEngine.Random.Range( 2, 6 );
            int sword = UnityEngine.Random.Range( 0, 3 );
            int shield = UnityEngine.Random.Range( 0, 3 );
            CharacterType type = (CharacterType)UnityEngine.Random.Range( 0, 3 );

            if( side == 0 )
                boardManager.SpawnHero( heart, sword, shield, type );
            else
                boardManager.SpawnEnemy( heart, sword, shield, type );
        }

        void OnAvatarCollideObject( Component component )
        {
            collidingObject = component;
        }

        IEnumerator ProcessHeroColliding( Hero hero )
        {
            yield return avatar.AddHero( hero );
        }

        IEnumerator ProcessEnemyColliding( Enemy enemy, Action<bool> onComplete )
        {
            Hero hero;
            int damage;
            while( true )
            {
                hero = avatar.GetLeadingHero( );
                Debug.Log( $"[COMBAT] HERO{hero} VS ENEMY{enemy}" );
                damage = hero.Attack( enemy );
                Debug.Log( $"[COMBAT] HERO inflicts {damage} damage to ENEMY{enemy}" );
                if( enemy.HasDied )
                {
                    Debug.Log( $"[COMBAT] ENEMY died" );
                    Destroy( enemy.gameObject );

                    avatar.SpeedUp( );

                    // grants points equal to total Hearts of the Avatar
                    score += avatar.TotalHeart;
                    scoreLabel.Score = score;

                    onComplete?.Invoke( false );
                    yield break;
                }
                yield return new WaitForSeconds( combatResolveTime );

                damage = enemy.Attack( hero );
                Debug.Log( $"[COMBAT] ENEMY inflicts {damage} damage to HERO{hero}" );
                if( hero.HasDied )
                {
                    Debug.Log( $"[COMBAT] HERO died" );
                    if( !avatar.TryRemoveTheFirstHero() )
                    {
                        onComplete?.Invoke( true );
                        yield break;
                    }
                }
                yield return new WaitForSeconds( combatResolveTime );
            }
        }

        IEnumerator HitTheWallAndRandomDirection( Tile wall, Action<bool, Direction> onComplete )
        {
            bool gameEnd = false;
            if( !avatar.TryRemoveTheLastHero( ) )
            {
                gameEnd = true;

                onComplete?.Invoke( gameEnd, Direction.Zero );
                yield break;
            }

            var direction = avatar.RandomDirection( );
            onComplete?.Invoke( gameEnd, direction );
        }
    }
}


