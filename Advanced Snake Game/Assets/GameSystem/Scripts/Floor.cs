﻿// Copyright (C) Nathakorn Soontornworachan. All rights reserved.
// Unauthorized copying of this file, via any medium is strictly prohibited.
// Proprietary and confidential. Authored for Advanced Snake Game by pop_nathakorn@hotmail.com.

using UnityEngine;

namespace GameSystem
{
    [RequireComponent(typeof( CircleCollider2D ) )]
    public class Floor : Tile
    {
        new CircleCollider2D collider;

        private void Awake( )
        {
            collider = GetComponent<CircleCollider2D>( );
        }

        public bool IsNearbyAvatar( )
        {
            LayerMask layerMask = LayerMask.GetMask( "Avatar" );
            bool isNearbyAvatar = collider.IsTouchingLayers( layerMask );

            return isNearbyAvatar;
        }

        public bool IsEmpty( )
        {
            LayerMask layerMask = LayerMask.GetMask( "Avatar", "HeroLine", "Hero", "Enemy" );

            var start = transform.position + Vector3.left.normalized * 0.49F;
            var end = transform.position + Vector3.right.normalized * 0.49F;

            var hit = Physics2D.Linecast( start, end, layerMask );

            return !hit;
        }
    }
}

