﻿// Copyright (C) Nathakorn Soontornworachan. All rights reserved.
// Unauthorized copying of this file, via any medium is strictly prohibited.
// Proprietary and confidential. Authored for Advanced Snake Game by pop_nathakorn@hotmail.com.

using UnityEngine;

public class GameInput : MonoBehaviour
{
    public int horizontal { get; private set; }
    public int vertical { get; private set; }

    private void Awake( )
    {
        horizontal = 0;
        vertical = 0;
    }

    private void Update( )
    {
        //Get input from the input manager, round it to an integer and store in horizontal to set x axis move direction
        horizontal = (int)( Input.GetAxisRaw( "Horizontal" ) );

        //Get input from the input manager, round it to an integer and store in vertical to set y axis move direction
        vertical = (int)( Input.GetAxisRaw( "Vertical" ) );

        //Check if moving horizontally, if so set vertical to zero.
        if( horizontal != 0 )
            vertical = 0;
    }
}
