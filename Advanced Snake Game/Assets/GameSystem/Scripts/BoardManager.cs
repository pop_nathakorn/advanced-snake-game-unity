﻿// Copyright (C) Nathakorn Soontornworachan. All rights reserved.
// Unauthorized copying of this file, via any medium is strictly prohibited.
// Proprietary and confidential. Authored for Advanced Snake Game by pop_nathakorn@hotmail.com.

using UnityEngine;
using System.Linq;
using System.Collections.Generic;

namespace GameSystem
{
    public class BoardManager : MonoBehaviour
    {
        public int Column = 21;         //Number of columns in our game board.
        public int Rows = 21;           //Number of rows in our game board.

        public GameObject Avatar;       //Avatar prefabs.
        public GameObject[] Heroes;     //Hero prefabs.
        public GameObject[] Enemies;    //Enemy prefabs.

        public GameObject[] WallTiles;  //Array of wall prefabs.
        public GameObject[] FloorTiles; //Array of floor prefabs.

        private Transform boardHolder;  //A variable to store a reference to the transform of our Board object.

        private Dictionary<Vector2, Tile> availableWallTiles;
        private Dictionary<Vector2, Floor> availableFloorTiles;

        //Sets up the outer walls and floor (background) of the game board.
        public void BoardSetup( )
        {
            //Instantiate Board and set boardHolder to its transform.
            boardHolder = new GameObject( "Board" ).transform;

            availableWallTiles = new Dictionary<Vector2, Tile>( );
            availableFloorTiles = new Dictionary<Vector2, Floor>( );

            //Loop along x axis, starting from -1 (to fill corner) with floor or outerwall edge tiles.
            for( int x = -1 ; x < Column + 1 ; x++ )
            {
                //Loop along y axis, starting from -1 to place floor or outerwall tiles.
                for( int y = -1 ; y < Rows + 1 ; y++ )
                {
                    GameObject toInstantiate;

                    //Check if we current position is at board edge
                    bool isBoardEdge = x == -1 || x == Column || y == -1 || y == Rows;
                    // if so choose a random outer wall prefab from our array of outer wall tiles.
                    if( isBoardEdge )
                        toInstantiate = WallTiles[Random.Range( 0, WallTiles.Length )];
                    //otherwise, choose a random tile from our array of floor tile prefabs and prepare to instantiate it.
                    else
                        toInstantiate = FloorTiles[Random.Range( 0, FloorTiles.Length )];

                    var tile = CreateTile( toInstantiate, new Vector3( x, y, 0f ) );

                    //Save reference in currentBoardTiles
                    if( tile is Floor )
                        availableFloorTiles[tile.transform.position] = tile as Floor;
                    else
                        availableWallTiles[tile.transform.position] = tile;
                }
            }
        }

        private Tile CreateTile( GameObject original, Vector3 position )
        {
            //Instantiate the GameObject instance using the prefab chosen for toInstantiate at the Vector3 corresponding to current grid position in loop, cast it to GameObject.
            GameObject instance = Instantiate( original, position, Quaternion.identity ) as GameObject;

            //Set the parent of our newly instantiated object instance to boardHolder, this is just organizational to avoid cluttering hierarchy.
            instance.transform.SetParent( boardHolder );

            return instance.GetComponent<Tile>( );
        }

        public Avatar SpawnAvatar( int heart, int sword, int shield, CharacterType type )
        {
            Vector3 position = new Vector3( Column / 2, Rows / 2, 0 );
            
            var avatarObject = Instantiate( Avatar, position, Quaternion.identity );
            var avatar = avatarObject.GetComponent<Avatar>( );

            var toInstantiate = Heroes[Random.Range( 0, Heroes.Length )];
            var heroObject = Instantiate( toInstantiate, position, Quaternion.identity );
            var hero = heroObject.GetComponent<Hero>( );
            hero.Heart = heart;
            hero.Sword = sword;
            hero.Shield = shield;
            hero.Type = type;
            hero.Refresh( );

            avatar.SetInitialHero( hero );

            return avatar;
        }

        public Hero SpawnHero( int heart, int sword, int shield, CharacterType type )
        {
            Vector2 position;
            if( !TryRandomAvailablePosition( out position ) )
                return null;

            var toInstantiate = Heroes[Random.Range( 0, Heroes.Length )];
            var heroObject = Instantiate( toInstantiate, position, Quaternion.identity );
            var hero = heroObject.GetComponent<Hero>( );
            hero.Heart = heart;
            hero.Sword = sword;
            hero.Shield = shield;
            hero.Type = type;
            hero.Refresh( );

            return hero;
        }

        public Enemy SpawnEnemy( int heart, int sword, int shield, CharacterType type )
        {
            Vector2 position;
            if( !TryRandomAvailablePosition( out position ) )
                return null;

            var toInstantiate = Enemies[Random.Range( 0, Heroes.Length )];
            var enemyObject = Instantiate( toInstantiate, position, Quaternion.identity );
            var enemy = enemyObject.GetComponent<Enemy>( );
            enemy.Heart = heart;
            enemy.Sword = sword;
            enemy.Shield = shield;
            enemy.Type = type;
            enemy.Refresh( );

            return enemy;
        }

        public bool TryRandomAvailablePosition( out Vector2 position )
        {
            position = Vector2.zero;
            var result = from keyPairValue in availableFloorTiles
                         where keyPairValue.Value.IsEmpty( ) && !keyPairValue.Value.IsNearbyAvatar( )
                         select keyPairValue.Value;

            List<Floor> availableTiles = result.ToList( );

            if( availableTiles.Count == 0 )
                return false;

            position = availableTiles[Random.Range( 0, availableTiles.Count )].transform.position;
            return true;
        }
    }

    public enum TileType
    {
        Floor,
        Wall
    }
}
